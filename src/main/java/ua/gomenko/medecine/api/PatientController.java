/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.gomenko.medecine.api;

//import javax.ws.rs.GET;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import ua.gomenko.model.Patient;

/**
 *
 * @author it260590gaa
 */
@Component
@RestController
@Path("/health")
public class PatientController {
    
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response health() {
        return Response.status(200).entity(new Patient("Jersey: Up and Running!")).build();
    }
}
