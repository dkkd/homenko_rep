/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.gomenko.config.app;


import java.util.Map;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

import ua.gomenko.medecine.api.PatientController;

/**
 *
 * @author it260590gaa
 */
@ApplicationPath("/*")
public class JerseyConfig extends ResourceConfig {
    
        public JerseyConfig() {
        register(PatientController.class);
    }

   
    
}
